import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import App from "App";
import { Store } from "global/Store";

function Component(route: any) {
  const dispatch = jest.fn();
  const state: any = {};
  return (
    <MemoryRouter initialEntries={[route]} initialIndex={0}>
      <Store.Provider
        value={{
          state,
          dispatch
        }}
      >
        <App />
      </Store.Provider>
    </MemoryRouter>
  );
}

it("test home component", () => {
  const { getByTestId } = render(Component("/"));

  expect(getByTestId("HomeComponent")).toBeTruthy();
});
